# GCC support can be specified at major, minor, or micro version
# (e.g. 8, 8.2 or 8.2.0).
# See https://hub.docker.com/r/library/gcc/ for all supported GCC
# tags from Docker Hub.
# See https://docs.docker.com/samples/library/gcc/ for more on how to use this image
FROM gcc:9.2.0

# Install required packages
RUN apt-get update && apt-get install -y \
    cmake

# Add Google Test
RUN git clone https://github.com/google/googletest.git /googletest \
    && mkdir -p /googletest/build \
    && cd /googletest/build \
    && cmake .. && make && make install \
    && cd / && rm -rf /googletest

# Add Boost
ENV BOOST_VERSION=1.72.0
ENV BOOST_VERSION_=1_72_0
ENV BOOST_ROOT=/usr/include/boost

RUN wget --max-redirect 3 https://dl.bintray.com/boostorg/release/${BOOST_VERSION}/source/boost_${BOOST_VERSION_}.tar.gz
RUN mkdir -p /usr/include/boost && tar zxf boost_${BOOST_VERSION_}.tar.gz -C /usr/include/boost --strip-components=1

RUN echo ${BOOST_ROOT}

# Copy the project into the container and set the working directory
COPY . /usr/src/cpp-dev
WORKDIR /usr/src/cpp-dev

# Compile the project
RUN make build build_test

LABEL Name=cpp-dev Version=0.0.1

ENTRYPOINT [ "/bin/bash" ]
