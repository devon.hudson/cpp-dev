# Test Project

This is a test project meant to showcase how to do full C++ development using Docker, Boost and Google Test.

## Requirements

Ensure Docker is installed on your computer.

## Build, Test & Run the Project

### Build

Run the following command:
`docker build --rm -f "Dockerfile" -t cppdev:latest "."`

### Test

Run the following command:
`docker run --rm --entrypoint ./build/TEST_APP cppdev:latest`

### Run

Run the following command:
`docker run --rm -it --entrypoint ./build/APP cppdev:latest`
