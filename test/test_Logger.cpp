#include "gtest/gtest.h"
#include "../src/Logger.h"
#include "mock_Logger.h"

class LoggerFixture: public ::testing::Test 
{ 
public: 
    LoggerFixture() 
    { 
        // initialization code here
    } 

    void SetUp() 
    { 
        // code here will execute just before the test ensues 
    }

    void TearDown() 
    { 
        // code here will be called just after the test completes
        // ok to through exceptions from here if need be
    }
};

TEST_F(LoggerFixture, loggerIsCalled)
{
    // Arrange
    MockLogger mockLogger;
    Logger logger(mockLogger);

    // Assert
    EXPECT_CALL(mockLogger, log("TEST"))
        .Times(1);

    // Act
    logger.log("TEST");
}
