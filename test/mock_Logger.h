#include "gmock/gmock.h"
#include "../src/ILogger.h"

class MockLogger : public ILogger 
{
public:
    MOCK_METHOD(void, log, (std::string), (override));
};
