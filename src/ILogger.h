#include <string>

class ILogger
{
public:
    virtual ~ILogger() {};

    virtual void log(std::string message) = 0;
};
