#include <string>
#include "ILogger.h"

class ConsoleLogger : public ILogger
{
public:
    ConsoleLogger() {};
    virtual ~ConsoleLogger() {};

    void log(std::string message) override;
};
