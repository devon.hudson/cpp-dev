#include <iostream>
#include <iterator>
#include <algorithm>
#include <memory>
#include <chrono>
#include <csignal>
#include <functional>

#include <boost/lambda/lambda.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/signals2.hpp>
#include <boost/function.hpp>

#include "ConsoleLogger.h"
#include "Logger.h"

namespace
{
  volatile std::atomic<bool> kShutdownApp;
}

void signalHandler(int signalNumber)
{
    std::cout << "Received signal: " << signalNumber << std::endl;
    std::cout << "Shutting down." << std::endl;

    kShutdownApp = true;
    std::exit(0);
}

int main(int argc, char const *argv[])
{
    std::signal(SIGINT, signalHandler);

    std::unique_ptr<ConsoleLogger> consoleLogger = std::make_unique<ConsoleLogger>();
    std::shared_ptr<Logger> logger = std::make_shared<Logger>(*consoleLogger.get());

    logger->log("Hello docker container!");
    logger->log("This is how you use docker for development!");

    boost::signals2::signal<void (std::string)> emitPrint;
    emitPrint.connect(boost::bind(&ConsoleLogger::log, consoleLogger.get(), _1));
    emitPrint("Boost signal usage");
 
    logger->log("Testing boost timer...");

    boost::asio::io_service ioservice;
    boost::asio::steady_timer timer{ioservice, std::chrono::seconds{3}};
    timer.async_wait([](const boost::system::error_code &ec)
        { std::cout << "3 sec\n"; });

    ioservice.run();

    logger->log("Enter some numbers:");
    typedef std::istream_iterator<int> in;
    std::for_each(in(std::cin), in(), std::cout << (boost::lambda::_1 * 3) << " " );

    return 0;
}

