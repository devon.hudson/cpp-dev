#include <string>

class ILogger;

class Logger
{
public:
    Logger(ILogger& logger) : mLogger(logger) {};
    virtual ~Logger() {};

    void log(std::string message);

private:
    ILogger& mLogger;
};
