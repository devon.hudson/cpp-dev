TARGET ?= APP
SRC_DIR ?= src

TEST_TARGET ?= TEST_APP
TEST_SRC_DIR ?= test

BUILD_DIR ?= build

SRCS := $(shell find $(SRC_DIR) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

TEST_SRCS := $(shell find $(TEST_SRC_DIR) -name *.cpp -or -name *.c -or -name *.s)
TEST_OBJS := $(TEST_SRCS:%=$(BUILD_DIR)/%.o)
TEST_OBJS += $(filter-out $(BUILD_DIR)/$(SRC_DIR)/Main.cpp.o,$(OBJS))
TEST_DEPS := $(TEST_OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIR) -type d)
INC_DIRS += ${BOOST_ROOT}
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP -pthread

TEST_LIBS ?= /usr/local/lib/libgtest.a /usr/local/lib/libgtest_main.a /usr/local/lib/libgmock.a

$(BUILD_DIR)/$(TARGET): $(OBJS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(OBJS) -o $@

$(BUILD_DIR)/$(TEST_TARGET): $(TEST_OBJS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(TEST_FLAGS) $(TEST_OBJS) -o $@ $(TEST_LIBS)

$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: all build build_test test clean
all: build build_test test

rebuild: clean all

build:
	@echo "Building project"
	make $(BUILD_DIR)/$(TARGET)

build_test:
	@echo "Building tests"
	make $(BUILD_DIR)/$(TEST_TARGET)

test:
	@echo "Running tests"
	./$(BUILD_DIR)/$(TEST_TARGET)

clean:
	$(RM) -r $(BUILD_DIR)
	
.DEFAULT_GOAL := all

-include $(DEPS)

MKDIR_P ?= mkdir -p
